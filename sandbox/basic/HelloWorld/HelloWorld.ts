// =================================================
//      Something
// =================================================

// define the class with constructor
class Author {
	constructor (public Name: string) { }
}

// create the instance of the class
let author = new Author("Kunal Chowdhury");

// print the information in the console
console.log("\nHello Readers,");
console.log("Thanks for visiting my blog");
console.log(`\t~ ${author.Name}`);


// =================================================
//      For Each
// https://www.tutorialspoint.com/typescript/typescript_array_foreach.htm
// =================================================
console.log("\n=== ForEach ====")
let num = [7, 8, 9];
num.forEach(function (value, index) {
  console.log(index + ": " + value);
}); 


// =================================================
//      Fat arrow
// https://www.tutorialsteacher.com/typescript/arrow-function
// =================================================
console.log("\n=== FatArray ====")
let sum = (x: number, y: number): number => {
	return x + y;
}

console.log(sum(10, 20)); //returns 30

// =================================================
//      Arrays
// https://www.typescriptlang.org/docs/handbook/basic-types.html
// =================================================
console.log("\n=== Array ====")
let print_array2d = (a: number[][]) => {
	a.forEach( (i, j) => {
		console.log(i + " " + j);
	});
};
let list: number[][] = [[1], [2,3], [7, 8]];
print_array2d(list);

// =================================================
//      Import and export
// =================================================
console.log("\n=== import export ====")
import {aRandomName} from './addTwoNumbers';
console.log( "2+3= " + aRandomName(2,3) );
// Only works for a module
// console.log( "2+3= " + addTwoNumbers(2,3) );
