// Classes are an ES6 addition which provides a familiar (from OO languages) short hand for creating functions populated with the prototype (methods in the class), static properties, and the constructor function, as well as helpers like `super` for calling to the super class.
// It also makes type information for classes much simpler to express in TypeScript.
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Clazz = (function () {
    // Use the `public` modifier to automatically set `this.myName = myName`. Only available in constructors, can also be `private` or `protected`.
    function Clazz(myName) {
        this.myName = myName;
        // Type `foo` must be specified before usage.
        this.foo = myName.toUpperCase();
    }
    // Methods are specified using this syntax of `<name> (<parameters>) { <body> }`.
    Clazz.prototype.method = function () {
        return true;
    };
    Object.defineProperty(Clazz.prototype, "value", {
        // You can also use getters as setters.
        get: function () {
            return this.foo;
        },
        enumerable: true,
        configurable: true
    });
    // Static methods are available on the constructor function.
    Clazz.method = function () {
        return 'wow, cool';
    };
    return Clazz;
})();
var c = new Clazz('hello');
c.myName; //=> "hello".
// c.foo //=> Error: Property 'foo' is protected and only accessible within class 'Clazz' and its subclasses.
c.value; //=> "HELLO".
Clazz.method(); //=> "wow, cool".
// ES6 extending classes. All this continues working when targeting ES5 and ES3 too!
var SubClazz = (function (_super) {
    __extends(SubClazz, _super);
    function SubClazz() {
        _super.call(this, 'wow'); // `super` calls the constructor we inherited from with the current context. In this case, it's `Clazz`.
        console.log(this.foo); // We can access `foo` too!
    }
    return SubClazz;
})(Clazz);
// Following the ES6 spec for classes, subclasses can access superclass static methods.
SubClazz.method(); //=> "wow, cool".
