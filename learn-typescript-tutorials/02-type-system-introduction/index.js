// Basic Types.
var isDone = true;
var height = 6;
var name = 'test';
var list = [1, 2, 3]; // Also: `var list: Array<number> = [1, 2, 3]` which is using generics.
// Enums.
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
var c = Color.Green;
// Any. Useful for those variables that we have no idea about.
var notSure = 4;
notSure = 'maybe a string';
notSure = false;
var anotherList = [1, true, 'free']; // Mix `any` with other types.
// Void. The absense of a type.
function warnUser() {
    alert('This is my warning message');
}
// Function Declaration with Types.
function add(a, b) {
    // return 'some string' //=> Error: Type 'string' is not assignable to type 'number'.
    return a + b;
}
// Optional Parameters.
function sayHello(name) {
    // Name is optional.
    if (name) {
        console.log('Hello ' + name + '!');
    }
    console.log('Hello');
}
// Variables
var x = 10; // Inferred 'number' type.
var y = 20; // Explicit 'number' type.
// Function calls.
add(x, y); // Works.
// add('a', 'b') //=> Error: Argument of type 'string' is not assignable to parameter of type 'number'.
// Type inferencing.
function takesCallback(cb) {
    return cb(new Error('Boom!'));
}
takesCallback(function (err) {
    console.log(err.message);
});
var foo = {};
foo.x = 10;
foo.y = 10;
// foo.anotherValue = 'test' //=> Error: Property 'anotherValue' does not exist on type 'Foo'.
// Not possible to do.
// var bad = { x: 10 } as string
// But we can force it to happen.
var bad = { x: 10 };
bad.toUpperCase(); // What?
function print() {
    return 'hello world';
}
print().toUpperCase(); //=> "HELLO WORLD".
function getLabel(obj) {
    return obj.label;
}
// Type guards.
function typeGuard(obj) {
    return typeof obj.bark === 'string';
}
var dog = { legs: 4, bark: 'woof!' };
if (typeGuard(dog)) {
    alert('It\'s a dog! ' + dog.bark); // Definitely a dog.
}
// Generics.
function identity(arg) {
    return arg;
}
function arrify(arr) {
    if (Array.isArray(arr)) {
        return arr;
    }
    return [arr];
}
var dictionary = {};
dictionary[name] = name;
// Union Types.
var value = 'test';
console.log(value.length); // Works because it exists on both `string` and `Array`.
// Intersection Types.
function extend(a, b) {
    Object.keys(b).forEach(function (key) {
        a[key] = b[key];
    });
    return a;
}
function makeDogFromAnimal(animal) {
    return extend(animal, { bark: 'woof woof woof' });
}
// Tuples.
var tuple = ['hello', 10];
// tuple = [10, 'hello'] //=> Error: Type '[number, string]' is not assignable to type '[string, number]'.
console.log(tuple[0].substr(1));
function eventually() {
    var ts = require('typescript');
}
// `typeof` only works on values, but you can use an interface with generics.
var Foo = (function () {
    function Foo() {
    }
    return Foo;
})();
function create(Clazz) {
    return new Clazz();
}
var result = create(Foo);
// `this`.
var Calculator = (function () {
    function Calculator(value) {
        if (value === void 0) { value = 0; }
        this.value = value;
    }
    Calculator.prototype.result = function () {
        return this.value;
    };
    Calculator.prototype.add = function (operand) {
        this.value += operand;
        return this;
    };
    Calculator.prototype.subtract = function (operand) {
        this.value -= operand;
        return this;
    };
    return Calculator;
})();
var x = new Calculator(10)
    .add(5)
    .result();
